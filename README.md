# funix_training
## week 1 2021-04-17 to 2021-04-24
**tritc**
- Overview for web backend: introduction, applications, constitution model
- Overview for working flows: models (e.g. MVC, MTV...), the relation between client(s) and server(s)
- Overview for database: SQL, NoSQL
- Overview for tools (Python programming): Socket, protobuf, zmq, flask, Django, multithread, multiprocess
- A little view for web front end: html, js
- Implement a simple Flask project
- *Output*: 
```
(1): Report
(2): Simple project, documentations
```
    
**namtp**
- Overview for (fire) smoke detection models
- Overview for few shot detection technology
- The contents structure are following:
```
- (1) Reference title
- (2) Purpose
- (3) Methodology
- (4) Data
- (5) Effective
```
- *Output*:
```
- (1) Valid data link (set)
- (2) Models **benchmarking**
- (3) Report
```
